#include "MotionManager.h"

void MotionManager::computeMotionRatio_color(int pixelDiffThreshold)
{
	assert(!inputFileNames.empty());

	motionRatio.clear();
	cv::Mat_<cv::Vec3b> currFrame;
	cv::Mat_<cv::Vec3b> previousFrame;
	cv::Mat_<cv::Vec3b> frameDiff;
	std::vector<cv::Mat> splittedFrameDiff;
	int numberOfNonZeros;
	int numberOfImagePixels;	
	size_t firstFrame, lastFrame;

	firstFrame = 0;
	lastFrame = inputFileNames.size()-1;
	for(size_t frameNo = firstFrame; frameNo <= lastFrame; ++frameNo)
	{
		// load color image
		cv::Mat_<cv::Vec3b> currFrame = cv::imread(inputFileNames[frameNo]);
		if(!currFrame.data)
		{
			std::cout<<"read image failed!"<<std::endl;
			motionRatio.push_back(-1.0);
			continue;
		}
		if(frameNo==firstFrame)
		{
			previousFrame = currFrame;
			numberOfImagePixels = currFrame.rows*currFrame.cols;
			continue;
		}
		cv::absdiff(currFrame,previousFrame,frameDiff);
		numberOfNonZeros = 0;
		cv::split(frameDiff,splittedFrameDiff);
		for (size_t i=0;i<splittedFrameDiff.size();++i)
		{
			cv::threshold(splittedFrameDiff[i],splittedFrameDiff[i], pixelDiffThreshold, 255, cv::THRESH_BINARY);
			numberOfNonZeros += cv::countNonZero(splittedFrameDiff[i]);
		}
		motionRatio.push_back((double)numberOfNonZeros/(double)numberOfImagePixels);

		previousFrame = currFrame;

		// Display image
		//cv::namedWindow("currFrame");
		//cv::imshow("currFrame",currFrame);
		//cv::namedWindow("frame difference");
		//cv::imshow("frame difference",frameDiff);
		std::cout<<"Frame "<<frameNo<<" is processed"<<std::endl;

		//cv::waitKey(20);
	}
	motionRatio.push_back(motionRatio.back());	// set the last element the same as its previous one
	std::cout<<"Frame "<<inputFileNames.size()<<" is processed"<<std::endl;
}

void MotionManager::computeMotionRatio(int pixelDiffThreshold)
{
	assert(!inputFileNames.empty());

	motionRatio.clear();
	cv::Mat currFrame;
	cv::Mat previousFrame;
	cv::Mat frameDiff;
	std::vector<cv::Mat> splittedFrameDiff;
	int numberOfNonZeros;
	int numberOfImagePixels;	
	size_t firstFrame, lastFrame;

	firstFrame = 0;
	lastFrame = inputFileNames.size()-1;
	for(size_t frameNo = firstFrame; frameNo <= lastFrame; ++frameNo)
	{
		// load color image
		currFrame = cv::imread(inputFileNames[frameNo],CV_LOAD_IMAGE_GRAYSCALE);
		if(!currFrame.data)
		{
			std::cout<<"read image failed!"<<std::endl;
			motionRatio.push_back(-1.0);
			continue;
		}
		if(frameNo==firstFrame)
		{
			previousFrame = currFrame;
			numberOfImagePixels = currFrame.rows*currFrame.cols;
			continue;
		}

		numberOfNonZeros = 0;
		cv::absdiff(currFrame, previousFrame, frameDiff);
		cv::threshold(frameDiff, frameDiff, pixelDiffThreshold, 255, cv::THRESH_BINARY);
		numberOfNonZeros += cv::countNonZero(frameDiff);
		motionRatio.push_back((double)numberOfNonZeros/(double)numberOfImagePixels);

		previousFrame = currFrame;

		// Display image
		//cv::namedWindow("currFrame");
		//cv::imshow("currFrame",currFrame);
		//cv::namedWindow("frame difference");
		//cv::imshow("frame difference",frameDiff);
		std::cout<<"Frame "<<frameNo<<" is processed"<<std::endl;

		//cv::waitKey(20);
	}
	motionRatio.push_back(motionRatio.back());	// set the last element the same as its previous one
	std::cout<<"Frame "<<inputFileNames.size()<<" is processed"<<std::endl;
}

 void MotionManager::clusterFiles(double motionRatioThreshold, size_t folderSizeThreshold, size_t gapSizeThreshold)
{
	std::vector<ImgFile> folder;
	std::vector< std::vector<ImgFile> >::iterator it;
	std::vector< std::vector<ImgFile> >::iterator it1,it2;
	size_t fr1, fr2;

	assert(inputFileNames.size() == motionRatio.size());
	// if motion ratio is larger than the thres, add the file name to folder
	// otherwise, if folder is not empty, add current folder to mainFolder, clear folder
	for(size_t i=0; i<motionRatio.size(); ++i)
	{		
		if(motionRatio[i] >= motionRatioThreshold)
		{
			folder.push_back(ImgFile(i,inputFileNames[i]));
			continue;
		}
	
		if(!folder.empty())
		{
			mainFolder.push_back(folder);
			folder.clear();
		}
	}

	// remove folders that contains very few files
	it = mainFolder.begin();
	while(it != mainFolder.end())
	{
		if (it->size() >= folderSizeThreshold)
			 ++it;
		else
			it = mainFolder.erase(it);
	}

	// connect folders if they are close to each other
	it1 = mainFolder.begin();
	it2 = it1;
	if (mainFolder.size() >= 1)	++it2;
	while(it2 != mainFolder.end() && it1 != mainFolder.end())
	{
		std::vector<ImgFile>::iterator tmp;
		fr1 = it1->back().idx;
		fr2 = it2->front().idx;
		if(fr2 - fr1 >= gapSizeThreshold)
		{
			++it1;
			++it2;
			continue;
		}

		// combine it1 and it2, including the gap between them
		for(size_t i=fr1+1; i<fr2; ++i)
		{
			it1->push_back(ImgFile(i,inputFileNames[i]));
		}
		it1->insert(it1->end(), it2->begin(), it2->end());
		it2 = mainFolder.erase(it2);
	}

}

 void MotionManager::copyFiles(std::string &outputDir)
 {
	 char temp[50];
	 std::string currFolder;
	 size_t numOfFolders;
	 size_t numOfFiles;
	 std::string cmd;

	 numOfFolders = mainFolder.size();
	 for(size_t i = 0; i < numOfFolders; ++i)
	 {
		 // Create directory
		 sprintf_s(temp,50,"\\_extracted_%03d",i+1);
		 currFolder = outputDir + temp;
		 if( _mkdir( currFolder.c_str() ) != 0 )
		 {
			 std::cout<<"Problem creating directory. Probablly it already exists."<<std::endl;
			 return;
		 }
		 // Copy file
		 numOfFiles = mainFolder[i].size();
		 for(size_t j = 0; j < numOfFiles; ++j)
		 {
			 cmd = "copy " + mainFolder[i][j].name + " " + currFolder;
			 system( cmd.c_str() );
		 }
	 }
 }

 void MotionManager::save_motionRatio(char *fileName)
 {
	 std::ofstream file;
	 file.open(fileName);
	 for (size_t i=0;i<motionRatio.size();++i)
	 {
		 file<<motionRatio[i]<<std::endl;
	 }
	 file.close();
 }

 void MotionManager::load_motionRatio(char *fileName)
 {
	 motionRatio.clear();
	 double temp;
	 std::ifstream infile;
	 std::cout<<"Loading motion ratio file ..."<<std::endl;
	 infile.open(fileName);
	 while (infile >> temp)
	 {
		 motionRatio.push_back(temp);
	 }
	 infile.close();
	 std::cout<<"Loading complete!"<<std::endl;
 }

 void MotionManager::save_inputFileNames(char *fileName)
 {
	 std::ofstream file;
	 file.open(fileName);
	 for (size_t i=0;i<inputFileNames.size();++i)
	 {
		 file<<inputFileNames[i]<<std::endl;
	 }
	 file.close();
 }

 void MotionManager::load_inputFileNames(char *fileName)
 {
	 inputFileNames.clear();
	 std::string temp;
	 std::ifstream infile;
	 std::cout<<"Loading inputFileNames file ..."<<std::endl;
	 infile.open(fileName);
	 while (infile >> temp)
	 {
		 inputFileNames.push_back(temp);
	 }
	 infile.close();
	 std::cout<<"Loading complete!"<<std::endl;
 }
