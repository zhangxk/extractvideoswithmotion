#ifndef MOTIONMANAGER_H
#define MOTIONMANAGER_H

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <direct.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

class ImgFile
{
public:
	ImgFile(int index=0, std::string str=""):idx(index),name(str){};
	int idx;
	std::string name;
};

class MotionManager
{
public:
	MotionManager(std::vector<std::string> &inputFiles=std::vector<std::string>()):inputFileNames(inputFiles) {};
	void computeMotionRatio_color(int pixelDiffThreshold=10);
	void computeMotionRatio(int pixelDiffThreshold=10);
	void clusterFiles(double motionRatioThreshold=0.001, size_t folderSizeThreshold=60, size_t gapSizeThreshold=200);
	void copyFiles(std::string &outputDir);

	void set_motionRatio(std::vector<double> &src) { motionRatio=src; };
	std::vector<double> get_motionRatio() { return motionRatio; };
	void save_motionRatio(char *fileName);
	void load_motionRatio(char *fileName);

	void set_inputFileNames(std::vector<std::string> &src) { inputFileNames=src; };
	std::vector<std::string> get_inputFileNames() { return inputFileNames; };
	void save_inputFileNames(char *fileName);
	void load_inputFileNames(char *fileName);

	void set_mainFolder(std::vector<std::vector<ImgFile> > &src) { mainFolder=src; };
	std::vector<std::vector<ImgFile> > get_mainFolder() { return mainFolder; };

private:
	std::vector<double> motionRatio;
	std::vector<std::string> inputFileNames;
	std::vector<std::vector<ImgFile> > mainFolder;
};

#endif // MOTIONMANAGER_H

