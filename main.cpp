// Function extractVideosWithMotion coded by Xikang Zhang, Northeastern University
// last modified in 12/24/2013,
// The code is to extract the video parts with apparent motion

#include <string>
#include "MotionManager.h"

std::vector<std::string> getFileNameList(std::string &inputDir, char * fileType);

int main(int argc, char **argv)
{
	std::string inputDir;
	std::string outputDir;
	size_t pixelDiffThreshold = 10;
	double motionRatioThreshold = 0.001;
	size_t folderSizeThreshold = 60;
	int gapSizeThreshold = 200;
	char * fileType = ".png";
	if (argc==1)
	{
		inputDir = ".\\testData";
		outputDir = ".\\output";
		pixelDiffThreshold = 10;
		motionRatioThreshold = 0.003;
		folderSizeThreshold = 5;
		gapSizeThreshold = 3;
		fileType = ".jpg";
	}
	else if (argc < 3)
	{ // Check the value of argc. If not enough parameters have been passed, inform user and exit.
		std::cout << "Usage is <inputDir> <outputDir> [-p pixelDiffThreshold \
					 -m motionRatioThreshold -f folderSizeThreshold -g gapSizeThreshold \
					 -t fileType ] \n"; // Inform the user of how to use the program
		std::cin.get();
		exit(0);
	} 
	else
	{
		inputDir = argv[1];
		outputDir = argv[2];
		for (int i = 3; i + 1 < argc; i = i + 2)
		{
			if (!strcmp(argv[i],"-p")) pixelDiffThreshold = atoi(argv[i+1]);
			else if (!strcmp(argv[i],"-m")) motionRatioThreshold = atof(argv[i+1]);
			else if (!strcmp(argv[i],"-f")) folderSizeThreshold = atoi(argv[i+1]);
			else if (!strcmp(argv[i],"-g")) gapSizeThreshold = atoi(argv[i+1]);
			else if (!strcmp(argv[i],"-t")) fileType = argv[i+1];
			else
			{
				std::cout << "Not enough or invalid arguments, please try again.\n";
				std::cin.get();
				exit(0);
			}
		}
	}

	//std::vector<std::string> inputFileNames = getFileNameList(inputDir,fileType);

	//MotionManager motionManager(inputFileNames);
	////motionManager.save_inputFileNames(".\\expData\\inputFileNames_temp");
	//motionManager.computeMotionRatio(pixelDiffThreshold);
	////motionManager.save_motionRatio(".\\expData\\motionRatio_temp");
	//motionManager.clusterFiles(motionRatioThreshold,folderSizeThreshold,gapSizeThreshold);
	//motionManager.copyFiles(outputDir);

	MotionManager motionManager;
	motionManager.load_inputFileNames(".\\expData\\video20121011cam1_inputFileNames.txt");
	motionManager.load_motionRatio(".\\expData\\video20121011cam1_motionRatio.txt");
	motionManager.clusterFiles(motionRatioThreshold,folderSizeThreshold,gapSizeThreshold);
	motionManager.copyFiles(outputDir);

	return 0;
}